﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Audio;

namespace BossMan
{
    [System.Serializable]
    public class CustomClip
    {
        [Header("Clip Settings")]
        public AudioClip Clip;
        public float Volume = 1f;
        [MinMaxRange(0f, 2f)]
        public Range Pitch = new Range() { Min = 1f, Max = 1f };
        public float Reverb = 1f;
        public bool Loop = false;
        public AudioRolloffMode Rolloff = AudioRolloffMode.Logarithmic;
        public AudioMixerGroup Group;

        public void Play(AudioSource audio)
        {
            audio.Stop();
            audio.clip = Clip;
            audio.volume = Volume;
            audio.pitch = UnityEngine.Random.Range(Pitch.Min, Pitch.Max);
            audio.loop = Loop;
            audio.reverbZoneMix = Reverb;
            audio.rolloffMode = Rolloff;
            audio.outputAudioMixerGroup = Group;
            audio.Play();
        }
    }

    public abstract class Singleton<T> where T : Singleton<T>, new()
    {
        public static T Instance
        {
            get
            {
                return Nested._instance;
            }
        }

        class Nested
        {
            static Nested()
            {

            }

            internal static readonly T _instance = new T();
        }
    }

    [System.Serializable]
    public class Range
    {
        public float Min;
        public float Max;
    }

    public class MinMaxRangeAttribute : Attribute
    {
        public MinMaxRangeAttribute(float min, float max)
        {
            Min = min;
            Max = max;
        }
        public float Min { get; private set; }
        public float Max { get; private set; }
    }
}
