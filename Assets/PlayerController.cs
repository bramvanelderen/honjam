﻿using System.Collections;
using UnityEngine;

namespace BossMan
{
	/// <summary>
	/// Handles death and cleanup
	/// </summary>
	public class PlayerController : PlayerComponent
	{
	    private PlayerMovement _movement;
	    private PlayerCollision _collision;
	    private PlayerStateMachine _state;
	    private PlayerAttack _attack;
	    private PlayerRoll _roll;

	    void Start()
	    {
	        _movement = GetComponent<PlayerMovement>();
	        _collision = GetComponent<PlayerCollision>();
	        _state = GetComponent<PlayerStateMachine>();
	        _attack = GetComponent<PlayerAttack>();
	        _roll = GetComponent<PlayerRoll>();

	    }

	    public void DestroyPlayer()
	    {
	        tag = "Untagged";
	        StartCoroutine(DisablePlayer());
	        Destroy(_movement);
	        Destroy(_attack);
	        Destroy(_roll);
	    }

	    IEnumerator DisablePlayer()
	    {
	        yield return new WaitUntil(IsGrounded);

	        _state.SwitchState(PlayerStateMachine.PlayerStates.Dead);
            var rb = GetComponent<Rigidbody2D>();
            rb.velocity = Vector3.zero;
            rb.gravityScale = 0;
            GetComponent<Collider2D>().isTrigger = true;
	        enabled = false;
	    }

	    bool IsGrounded()
	    {
	        return _collision.IsGrounded;
	    }
    }
}

