﻿using System;
using UnityEngine;

namespace BossMan
{
    /// <summary>
    /// Health data
    /// </summary>
    [CreateAssetMenu(fileName = "HealthData", menuName = "PowerUp/Health")]
    public class PuHealthData : PowerUpData
    {
        public float AdditiveHealth = 10f;

        protected override void ApplyPowerUp(GameObject obj, float power)
        {

            obj.SendMessage("UpgradeHealth", AdditiveHealth*power);

        }
    }
}

