﻿using UnityEngine;
using UnityEngine.UI;

namespace BossMan
{
    /// <summary>
    /// Manages message text field
    /// </summary>
    [RequireComponent(typeof(Text))]
    public class MessageManager : MonoBehaviour
	{
        private Text _textField;

	    void Start()
	    {
	        _textField = GetComponent<Text>();
	        SetMessage("Slay The Daemon", 1.5f);

	    }

	    public void SetMessage(string message, float lifetime = 1f)
	    {
            CancelInvoke("Reset");
            _textField.text = message;
            Invoke("Reset", lifetime);
	    }

	    void Reset()
	    {
	        _textField.text = "";
	    }
	}
}

