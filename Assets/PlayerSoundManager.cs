﻿using UnityEngine;

namespace BossMan
{
	/// <summary>
	/// 
	/// </summary>
	public class PlayerSoundManager : MonoBehaviour {

        public CustomClip Clip;
        private AudioSource _audio;

        void Start()
        {
            _audio = gameObject.AddComponent<AudioSource>();
        }

        public void OnFootstep()
        {
            Clip.Play(_audio);
        }

	}
}

