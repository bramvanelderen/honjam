﻿using UnityEngine;

namespace BossMan
{
    public class PlayerComponent : MonoBehaviour
    {
        public virtual void RemovePlayerComponent()
        {
            Destroy(this);
        }

    }
}
