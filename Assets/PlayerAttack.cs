﻿using UnityEngine;

namespace BossMan
{
	/// <summary>
	/// 
	/// </summary>
	public class PlayerAttack : MonoBehaviour
	{
	    [SerializeField] private float FireRate = 1f;
	    private float _timeLastShot = 0f;

	    private PlayerMovement _movement;
	    private ThrowObject _throw;

	    void Start()
	    {
	        _movement = GetComponent<PlayerMovement>();
	        _throw = GetComponent<ThrowObject>();

	    }

	    void Update()
	    {
            if (Input.GetButtonDown("Fire1") && Time.time > _timeLastShot)
            {
                var offset = Vector3.zero;
                offset.x = 1.5f * ((_movement.PlayerDirection == Direction.Right) ? 1 : -1);
                offset.y = 2f;
                var dir = (_movement.PlayerDirection == Direction.Right) ? Vector3.right : Vector3.left;
                dir.y = .5f;
                _throw.Throw(transform.position + offset, dir);

                _timeLastShot = Time.time + FireRate;
            }
        }

	    public void UpgradeFireRate(float amount)
	    {
	        FireRate -= amount;
	    }

	}
}

