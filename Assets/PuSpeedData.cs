﻿using System;
using UnityEngine;

namespace BossMan
{
    /// <summary>
    /// Health data
    /// </summary>
    [CreateAssetMenu(fileName = "SpeedData", menuName = "PowerUp/Speed")]
    public class PuSpeedData : PowerUpData
    {
        public float AdditiveSpeed = 2f;

        protected override void ApplyPowerUp(GameObject obj, float power)
        {

            obj.SendMessage("UpgradeSpeed", AdditiveSpeed * power);

        }
    }
}

