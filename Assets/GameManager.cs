﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

namespace BossMan
{
	/// <summary>
	/// Manages the whole game
	/// </summary>
	[RequireComponent(typeof(PowerUpManager))]
	public class GameManager : MonoBehaviour
	{
	    [SerializeField] private GameObject _menuPanel;
	    [SerializeField] private GameObject _gamePanel;
	    [SerializeField] private GameObject _controlsPanel;
	    [SerializeField] private GameObject _helpPanel;
	    [SerializeField] private GameObject _pausePanel;

        [SerializeField] private Image _bossBar;
	    [SerializeField] private Image _playerBar;

	    [SerializeField] private GameObject _playerPrefab;
        [SerializeField] private GameObject _bossPrefab;
	    [SerializeField] private GameObject _bossPositions;
	    [SerializeField] private List<GameObject> SpawnPoints = new List<GameObject>();
	    [SerializeField] private float _respawnDelay = 2f;

	    private GameObject _player;
	    private PowerUpManager _pum;
	    private MusicManager _mm;
	    private float _power = 1f;
	    private Health _bossHealth;

	    private bool _ingame = false;
	    private bool _paused = false;

	    void Start()
	    {
            Cursor.lockState = CursorLockMode.None;
            _mm = GetComponent<MusicManager>();
            _menuPanel.SetActive(true);
            _gamePanel.SetActive(false);
	        _pausePanel.SetActive(false);
            _controlsPanel.SetActive(false);
            _helpPanel.SetActive(false);

        }

	    void Update()
	    {
	        if (_ingame)
	        {
	            if (Input.GetKeyDown(KeyCode.Escape))
	            {
	                if (_paused)
	                {
	                    Resume();
	                }
	                else
	                {
	                    Pause();
	                }
	            }


	        }

	    }

	    public void StartGame()
	    {
            Cursor.lockState = CursorLockMode.Locked;
            _mm.StartGameMusic();
            _menuPanel.SetActive(false);
	        _gamePanel.SetActive(true);
            _pum = GetComponent<PowerUpManager>();
            CreatePlayer();
            CreateBoss();
	        _ingame = true;
	    }

	    public void OpenControls()
	    {
            _menuPanel.SetActive(false);
            _controlsPanel.SetActive(true);
        }

	    public void CloseControls()
	    {
            _menuPanel.SetActive(true);
            _controlsPanel.SetActive(false);
        }

        public void OpenHelp()
        {
            _menuPanel.SetActive(false);
            _helpPanel.SetActive(true);
        }

        public void CloseHelp()
        {
            _menuPanel.SetActive(true);
            _helpPanel.SetActive(false);
        }

	    public void Pause()
	    {
	        _paused = true;
	        _pausePanel.SetActive(true);
	        Time.timeScale = 0;
            Cursor.lockState = CursorLockMode.None;
        }

	    public void Resume()
	    {
            _paused = false;
            _pausePanel.SetActive(false);
            Time.timeScale = 1;
            Cursor.lockState = CursorLockMode.Locked;
        }

	    public void MainMenu()
        {
            Time.timeScale = 1;
            SceneManager.LoadScene("Main");
	    }

        public void ExitGame()
	    {
            Time.timeScale = 1;
            Application.Quit();
	    }

	    void CreateBoss()
	    {
	        var boss = Instantiate(_bossPrefab);
	        var obj = _bossPositions.transform.GetChild(Random.Range(0, _bossPositions.transform.childCount));
	        boss.transform.position = obj.transform.position;

	        var hp = boss.GetComponent<Health>();
	        hp.HealthBar = _bossBar;
	        hp.AddOnDeathListener(BossIsDead);
	        _bossHealth = hp;
	        _bossHealth.Del = _pum.UpdatePower;

            var comp = boss.GetComponent<BossFly>();
	        comp.positions = _bossPositions;

	    }

	    void CreatePlayer()
	    {
	        if (_player)
	        {
	            _pum.TransformObjToPowerUp(_player);
	            _player = null;
	        }

	        if (_bossHealth)
	        {
                _bossHealth.UpgradeHealth(0);
            }
            

            var spawn = Vector3.zero;
	        if (SpawnPoints.Count > 1)
	            spawn = SpawnPoints[Random.Range(0, SpawnPoints.Count)].transform.position;
	        _player = Instantiate(_playerPrefab, spawn, Quaternion.Euler(Vector3.zero));

	        var health = _player.GetComponent<Health>();
	        health.HealthBar = _playerBar;
	        health.AddOnDeathListener(PlayerIsDead);

	    }

	    public void PlayerIsDead()
	    {
            _pum.CleanUp();
            //create new player
	        CancelInvoke("CreatePlayer");
	        Invoke("CreatePlayer", _respawnDelay);
	    }

	    public void BossIsDead()
	    {
	        SceneManager.LoadScene("Main");
	    }

	    public void UpdatePower(float percentage)
	    {
	        _power = percentage;
	    }
	}
}

