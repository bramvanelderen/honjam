﻿using UnityEngine;
using System.Collections;

namespace BossMan
{
    public enum Direction
    {
        Left,
        Right
    }

    /// <summary>
    /// Handles all input for the movement
    /// </summary>
    [RequireComponent(typeof(PlayerCollision), typeof(PlayerStateMachine), typeof(Rigidbody2D))]
    public class PlayerMovement : PlayerComponent
    {
        [SerializeField, Range(4f, 20f)] private float _movementSpeed = 10f;
        [SerializeField, Range(8f, 20f)] private float _jumpSpeed = 10f;
        [SerializeField] private float _inAirForce = 500f;
        [SerializeField] private float _wallPushForce = 300f;
        private PlayerCollision _collision;
        private PlayerStateMachine _state;
        private Rigidbody2D _rb2d;
        private float _inputWeight = 1f;
        private Direction _playerDirection = Direction.Right;

        private bool _isJumping = false;

        public Direction PlayerDirection
        {
            get { return _playerDirection; }
        }

        void Start()
        {
            _collision = GetComponent<PlayerCollision>();
            _rb2d = GetComponent<Rigidbody2D>();
            _state = GetComponent<PlayerStateMachine>();
        }

        void Update()
        {
            if (_isJumping && _collision.IsGrounded)
            {
                _isJumping = false;
                _state.SwitchState(PlayerStateMachine.PlayerStates.Idle);
            }

            if (Input.GetButtonDown("Jump"))
                Jump();
        }

        void FixedUpdate()
        {
            AdjustInputWeight();

            if (!_state.CanMove)
                return;

            var horizontal = Input.GetAxis("Horizontal");
            if (horizontal < -.1)
            {
                _playerDirection = Direction.Left;
                transform.localScale = new Vector3(-1f, 1f, 1f);
            } else if (horizontal > .1)
            {
                _playerDirection = Direction.Right;
                transform.localScale = new Vector3(1f, 1f, 1f);
            }
            _state.Speed = Mathf.Abs(horizontal);

            var vel = _rb2d.velocity;
            vel.x = ApplyWeight(vel.x, horizontal * _movementSpeed, _inputWeight);
            _rb2d.velocity = vel;
            
        }

        void Jump()
        {
            var vel = _rb2d.velocity;
            var force = Vector3.zero;
            if (_collision.IsGrounded)
            {
                vel.y = _jumpSpeed;
            }
            else if (_collision.WallDetected(PlayerDirection))
            {
                vel.y = _jumpSpeed;
                force.x = _wallPushForce * ((PlayerDirection == Direction.Right) ? -1 : 1);
                _inputWeight = 0f;
            }
            _state.SwitchState(PlayerStateMachine.PlayerStates.Jump);
            _isJumping = true;

            _rb2d.velocity = vel;
            _rb2d.AddForce(force);

        }

        float ApplyWeight(float start, float target, float weight)
        {
            var result = start;
            var difference = Mathf.Abs(start - target);
            if (target > start)
            {
                result += difference*weight;
            }
            else
            {
                result -= difference*weight;
            }

            return result;
        }

        void AdjustInputWeight()
        {
            if (_inputWeight == 1f)
                return;
            _inputWeight += 1f*Time.deltaTime;
            if (_inputWeight > 1f)
                _inputWeight = 1f;
        }

        public void UpgradeSpeed(float amount)
        {
            _movementSpeed += amount;
        }

        public void UpgradeJump(float amount)
        {
            _jumpSpeed += amount;
        }
    }
}


