﻿using UnityEngine;
using System.Collections.Generic;

namespace BossMan
{
	/// <summary>
	/// 
	/// </summary>
	public class PowerUpManager : MonoBehaviour
	{
	    [SerializeField] private CustomClip _pickUpClip;
	    [SerializeField] private CustomClip _destroyClip;

        [SerializeField] private int _maxPowerups = 4;
        private List<GameObject> PowerUps = new List<GameObject>();


        [SerializeField] private List<PowerUpData> PowerUpDataCollection;
	    [SerializeField] private MessageManager Mm;

	    private float _power = 1f;

	    public void TransformObjToPowerUp(GameObject obj)
	    {
            
	        var powerUp = obj.AddComponent<PowerUp>();
	        powerUp.Power = _power;
	        powerUp.Mm = Mm;
	        powerUp.PickUpClip = _pickUpClip;
	        powerUp.DestroyClip = _destroyClip;

            if (PowerUpDataCollection.Count > 0)
	            powerUp.data = PowerUpDataCollection[Random.Range(0, PowerUpDataCollection.Count)];

	        PowerUps.Add(obj);

	        CleanList(PowerUps);
        }

        public void UpdatePower(float percentage)
        {
            _power = percentage;
        }

	    public void CleanUp()
	    {
	        CleanList(PowerUps);

	        RemoveExcessPowerUps(PowerUps, _maxPowerups);

	        foreach (var obj in PowerUps)
	        {
	            var comp = obj.GetComponent<PowerUp>();
	            comp.IsUsed = false;
	        }

	    }

	    void RemoveExcessPowerUps(List<GameObject> powerUps, int max)
	    {
            var excessPuCount = powerUps.Count - max;
            if (excessPuCount > 0)
            {
                var powerups = new List<GameObject>();
                for (var i = 0; i < excessPuCount; i++)
                {
                    powerups.Add(powerUps[Random.Range(0, powerUps.Count)]);
                }

                foreach (var obj in powerups)
                {
                    PowerUps.Remove(obj);
                    Destroy(obj);
                }
            }
        }

	    void CleanList<T>(List<T> list)
	    {
            //Reverse cleanup list
            for (var i = list.Count - 1; i > -1; i--)
            {
                if (list[i] == null)
                    list.RemoveAt(i);
            }
        }

        PowerUpData GetRandomPowerUp()
        {
            if (PowerUpDataCollection.Count == 0)
                return null;

            return PowerUpDataCollection[Random.Range(0, PowerUpDataCollection.Count)];
        }
	}
}

