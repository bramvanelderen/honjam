﻿using UnityEngine;

namespace BossMan
{
	/// <summary>
	/// Throw object script, can be attached to any object
	/// </summary>
	public class ThrowObject : MonoBehaviour
    {
        [SerializeField] private GameObject _objectPrefab;
	    [SerializeField] private CustomClip _hitClip;

	    [SerializeField] private float _throwSpeed;
	    [SerializeField] private float _rotationSpeed;
	    [SerializeField] private float _damage;
	    [SerializeField] private float _gravityScale;

        public void Throw(Vector3 origin, Vector3 direction, bool isTrigger = false, Quaternion rotation = new Quaternion())
        {
            var obj = Instantiate(_objectPrefab);
            obj.transform.position = origin;
            obj.transform.rotation = rotation;
            var proj = obj.AddComponent<Projectile>();
            var rb2d = proj.Initialise(transform, _hitClip, _rotationSpeed, _damage, .5f, _gravityScale, isTrigger);
            if (!rb2d)
            {
                Destroy(obj);
            }
            else
            {
                rb2d.velocity = direction*_throwSpeed;
            }
        }

	    public void UpgradeDamage(float amount)
	    {
	        _damage += amount;
	    }

	    public void UpgradeSpeed(float amount)
	    {
	        _throwSpeed += amount;
	    }
	}
}

