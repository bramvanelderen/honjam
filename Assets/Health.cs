﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace BossMan
{

    /// <summary>
    /// Health component for both boss monster and player
    /// </summary>
    public class Health : MonoBehaviour
    {
        public delegate void InversePercentageHealth(float percentage);

        public InversePercentageHealth Del;
        public Image HealthBar;
        [SerializeField] private float _maxHp = 10f;
        [SerializeField] private UnityEvent _onDeath;
        private float _currentHealth;

        private bool _invinsible = false;

        public bool Alive
        {
            get { return (_currentHealth > 0); }
        }

        void Awake()
        {
            _currentHealth = _maxHp;
        }

        void Update()
        {
            if (Del != null)
            {
                Del(1f - (_currentHealth / _maxHp));
            }
                

            if (HealthBar)
            {
                HealthBar.fillAmount = _currentHealth/_maxHp;
            }
        }

        public float GetPercentageHp()
        {
            return (_currentHealth/_maxHp);
        }

        public void Damage(float amount)
        {
            if (_invinsible)
                return;

            if (_currentHealth <= 0)
                return;

            _currentHealth -= amount;
            if (_currentHealth <= 0)
            {
                if (HealthBar)
                    HealthBar.fillAmount = 0;
                _currentHealth = 0;
                if (_onDeath != null)
                    _onDeath.Invoke();

                Destroy(this);
            }
        }

        public void SetInvincible(bool active)
        {
            _invinsible = active;
        }

        public void UpgradeHealth(float amount)
        {
            _maxHp += amount;
            _currentHealth = _maxHp;
        }

        public void Heal(float amount)
        {
            if (!Alive)
                return;
            _currentHealth += amount;
            if (_currentHealth > _maxHp)
                _currentHealth = _maxHp;
        }

        public void AddOnDeathListener(UnityAction call)
        {
            _onDeath.AddListener(call);
        }
    }
}

