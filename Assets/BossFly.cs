﻿using UnityEngine;
using System.Collections.Generic;

namespace BossMan
{
	/// <summary>
	/// 
	/// </summary>
	public class BossFly : MonoBehaviour
	{
	    public GameObject positions;

	    [SerializeField] private Vector3 _scale = new Vector3(4, 4, 4);
	    [SerializeField] private AnimationCurve _curve = new AnimationCurve(
	        new Keyframe(0f, 1f),
	        new Keyframe(0.5f, .5f),
	        new Keyframe(1f, 1f));

        [SerializeField]
        private AnimationCurve _jumpCurve = new AnimationCurve(
            new Keyframe(0f, 0f),
            new Keyframe(0.5f, .2f),
            new Keyframe(.8f, 1f),
            new Keyframe(1f, 0f));

        [SerializeField]
        private AnimationCurve _difficulty = new AnimationCurve(
            new Keyframe(0f, 1f),
            new Keyframe(0.5f, 2f),
            new Keyframe(1f, 10f));

	    [SerializeField] private float _attackIntervalMin = .2f;
	    [SerializeField] private float _attackIntervalMax = 2f;
        [SerializeField] private float _jumpInterval = 30f;
	    [SerializeField] private float _speed = 15f;

	    [SerializeField] private CustomClip _clipJump;
	    [SerializeField] private CustomClip _clipAttack;

        private float _time = 0f;
	    private ThrowObject _throw;

	    private bool _isJumping = false;
	    private float _timeNextJump = 0f;
	    private float _timeNextAttack = 0f;

	    private GameObject target = null;
	    private float totalDistance = 0f;
	    private Vector3 realPos = Vector3.zero;

	    private Health _hp;
	    private bool _isdead = false;
	    private BossStateMachine _state;
	    private bool _isAttacking = false;

	    private AudioSource _audio1;
	    private AudioSource _audio2;

	    void Start()
	    {
	        _audio1 = gameObject.AddComponent<AudioSource>();
	        _audio2 = gameObject.AddComponent<AudioSource>();

            _hp = GetComponent<Health>();
	        _state = GetComponent<BossStateMachine>();

            _throw = GetComponent<ThrowObject>();
	        _timeNextAttack = Time.time + _attackIntervalMax;

	        realPos = transform.position;

	    }

	    void Update()
	    {
	        Animation();
	        Jump();
	        Attack();
            if (!_isJumping)
	            transform.position = Vector3.MoveTowards(transform.position, realPos, _speed*Time.deltaTime);
	    }



	    void Attack()
	    {
	        if (Time.time > _timeNextAttack && !_isdead && !_isJumping && !_isAttacking)
	        {
	            _isAttacking = true;
	            _state.SwitchState(BossStateMachine.BossStates.Attack);
	            Invoke("AttackForReal", 0.2f);
            }
        }

	    void AttackForReal()
	    {
            var projectileCount = Random.Range(4, 10);
            var offset = Random.Range(0, 180);
            var interval = 360f / projectileCount;
            var value = 0f;
            while (value < 360f)
            {
                var quat = Quaternion.Euler(0, 0, value + offset);
                var dir = quat * Vector3.up;

                _throw.Throw(transform.position + new Vector3(0, 2f, 0), dir, true, quat);

                value += interval;
            }
	        _clipAttack.Play(_audio1);
	        Invoke("StopAttack", 0.05f);
	    }

	    void StopAttack()
	    {
            _state.SwitchState(BossStateMachine.BossStates.Idle);
	        _isAttacking = false;
            _timeNextAttack = Time.time + Random.Range(_attackIntervalMin, _attackIntervalMax);
        }

	    void Jump()
	    {
	        if (_isJumping)
	        {
	            realPos = Vector3.MoveTowards(realPos, target.transform.position,
	                _speed*Time.deltaTime);

                var percentage = Vector3.Distance(target.transform.position, transform.position)/totalDistance;
	            var pos = realPos;
                pos.y += (_jumpCurve.Evaluate(1f - percentage)* 3);
	            transform.position = pos;

	            if (Vector3.Distance(target.transform.position, realPos) < .2f)
	            {
	                _isJumping = false;
                    _timeNextJump = Time.time + (_jumpInterval/_difficulty.Evaluate((1f - _hp.GetPercentageHp())));
	                _state.SwitchState(BossStateMachine.BossStates.Idle);
	            }

	        }
	        else if (!_isdead && !_isAttacking)
	        {
	            if (Time.time > _timeNextJump)
	            {
	                target = positions.transform.GetChild(Random.Range(0, positions.transform.childCount)).gameObject;
	                totalDistance = Vector3.Distance(target.transform.position, transform.position);
	                realPos = transform.position;
	                _state.SwitchState(BossStateMachine.BossStates.Jump);
	                _clipJump.Play(_audio2);
                    _isJumping = true;
	            }

	        }
	    }

	    void Animation()
	    {
            _time += Time.deltaTime * 2;
            if (_time >= 1f)
            {
                _time = 0f;
            }

            var scale = Vector3.zero;

            scale.y = _curve.Evaluate(_time);
            transform.localScale = _scale * scale.y;
        }

	    public void Kill()
	    {
	        CancelInvoke("AttackForReal");
	        CancelInvoke("StopAttack");
            Destroy(gameObject);
	    }
	}
}

