﻿using UnityEngine;

namespace BossMan
{
    /// <summary>
    /// Attach to any object to turn it into a projectile
    /// </summary>
    public class Projectile : MonoBehaviour
    {
        private Transform _origin;
        private float _damage = 1f;
        private Rigidbody2D _rb2d;
        private Vector3 _rotationDirection = Vector3.zero;
        private float _rotationSpeed = 360f;
        private CustomClip _clip;

        void Start()
        {
        }

        void Update()
        {
            transform.Rotate(_rotationDirection * _rotationSpeed * Time.deltaTime);
        }

        public Rigidbody2D Initialise(Transform origin = null, CustomClip clip = null, float rotationSpeed = 360f, float damage = 1f, float radius = .5f, float gravityScale = 3f, bool isTrigger = false)
        {
            _clip = clip;
            Invoke("DestroyProjectile", 10);
            _origin = origin;
            _rotationDirection = (new Vector3(0, 0, Random.Range(0f, 1f))).normalized;
            _rotationSpeed = rotationSpeed;
            _damage = damage;

            var collider = GetComponent<Collider2D>();
            if (!collider)
            {
                var circle = gameObject.AddComponent<CircleCollider2D>();
                circle.radius = radius;
                collider = circle;
            }
            collider.isTrigger = isTrigger;


            _rb2d = GetComponent<Rigidbody2D>();
            if (!_rb2d)
                _rb2d = gameObject.AddComponent<Rigidbody2D>();
            _rb2d.gravityScale = gravityScale;

            return _rb2d;
        }

        void OnCollisionEnter2D(Collision2D collision)
        {
            OnHit(collision.collider, true);
        }

        public void OnTriggerEnter2D(Collider2D collision)
        {
            OnHit(collision, false);
        }

        void OnHit(Collider2D collision, bool destroy = false)
        {
            if (collision.transform == transform || collision.transform == _origin)
                return;
            
            collision.transform.SendMessage("Damage", _damage, SendMessageOptions.DontRequireReceiver);
            if (destroy)
                DestroyProjectile();
        }

        void DestroyProjectile()
        {
            var obj = new GameObject();
            Destroy(obj, 2f);
            _clip.Play(obj.AddComponent<AudioSource>());
            Destroy(gameObject);
        }
    }
}

