﻿using UnityEngine;

namespace BossMan
{
	/// <summary>
	/// 
	/// </summary>
	public class MusicManager : MonoBehaviour {
        [SerializeField]
        private CustomClip _menu;
        [SerializeField]
        private CustomClip _game;

        private AudioSource _audio;

        [SerializeField]
        private bool _silent = false;

        void Start()
        {
            _audio = gameObject.AddComponent<AudioSource>();
            if (!_silent)
                _menu.Play(_audio);
        }

        public void StartMenuMusic()
        {
            if (!_silent)
                _menu.Play(_audio);
        }

        public void StartGameMusic()
        {
            if (!_silent)
                _game.Play(_audio);
        }

	}
}

