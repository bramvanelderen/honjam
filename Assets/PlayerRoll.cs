﻿using UnityEngine;

namespace BossMan
{
	/// <summary>
	/// 
	/// </summary>
	public class PlayerRoll : MonoBehaviour
	{
        [SerializeField] private float _duration = .5f;
        [SerializeField] private float _invincibilityPercentage = .5f;
	    [SerializeField] private float _speed = 9f;

        private PlayerStateMachine _state;
	    private PlayerMovement _movement;
	    private Rigidbody2D _rb;

	    private bool _isNotRolling = true;

	    void Start()
	    {
	        _state = GetComponent<PlayerStateMachine>();
	        _movement = GetComponent<PlayerMovement>();
            _rb = GetComponent<Rigidbody2D>();

	    }

	    void Update()
	    {
            if (Input.GetButtonDown("Roll") && !_state.IsRolling && _isNotRolling)
            {
                Roll();
            }
	    }

	    void FixedUpdate()
	    {
	        if (!_isNotRolling)
	        {
	            var vel = _rb.velocity;
	            vel.x = _speed * ((_movement.PlayerDirection == Direction.Right)? 1f: -1f);
	            _rb.velocity = vel;
	        }
	    }

	    void LateUpdate()
	    {
	        Restore();
	    }

	    void Roll()
	    {
	        SendMessage("SetInvincible", true);
	        _isNotRolling = false;
            _state.SwitchState(PlayerStateMachine.PlayerStates.Roll);

	        Invoke("EndRoll", _duration);
	        Invoke("RestoreInvinsibility", _duration*_invincibilityPercentage);
	        Invoke("ReturnIdle", _duration);
	    }

	    void EndRoll()
	    {
	        _isNotRolling = true;
	    }

	    void RestoreInvinsibility()
	    {
            SendMessage("SetInvincible", false);
        }

	    void ReturnIdle()
	    {
	        _state.SwitchState(PlayerStateMachine.PlayerStates.Idle);
	    }

	    void Restore()
	    {
            if (!_state.IsRolling)
            {
                CancelInvoke("ReturnIdle");
                CancelInvoke("RestoreInvinsibility");
                if (!_isNotRolling)
                {
                    RestoreInvinsibility();
                    EndRoll();
                }

            }
        }

	    void OnDestroy()
	    {
	        Restore();
	    }

	}
}

