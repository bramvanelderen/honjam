﻿using UnityEngine;
using System.Collections.Generic;

namespace BossMan
{
    /// <summary>
    /// Checks for playercollision
    /// </summary>
    public class PlayerCollision : PlayerComponent
    {
        [SerializeField] private List<string> _ignoreTags = new List<string>();
        [SerializeField] private LayerMask _interactable;
        [SerializeField] private float _colliderRadius = .5f;

        [Header("Ground Check")]
        [SerializeField, Range(-.5f, 0f)] private float _offset = 0.01f;

        [Header("Wall Check")]
        [SerializeField]private Vector3 _offsetWall = new Vector3(.15f, .1f, 0f);

        public bool IsGrounded
        {
            get
            {
                var isGrounded = false;
                var offset = new Vector3(0f, _offset + _colliderRadius, 0f);
                foreach (var obj in Physics2D.OverlapCircleAll(transform.position + offset, _colliderRadius, _interactable))
                {
                    if (_ignoreTags.Contains(obj.tag))
                    {
                        continue;
                    }
                    else
                    {
                        isGrounded = true;
                        break;
                    }
                }

                return isGrounded;
            }
        }

        public bool WallDetected(Direction dir)
        {
            var offset = _offsetWall;
            if (dir == Direction.Left)
                offset.x *= -1f;
            offset.y += _colliderRadius;

            var wall = false;
            foreach (var obj in Physics2D.OverlapCircleAll(transform.position + offset, _colliderRadius, _interactable))
            {
                if (_ignoreTags.Contains(obj.tag))
                {
                    continue;
                }
                else
                {
                    wall = true;
                    break;
                }
            }

            return wall;
        }
    }
}

