﻿using UnityEngine;
using UnityEngine.UI;

namespace BossMan
{
	/// <summary>
	/// Holds data for powerup
	/// </summary>
	public abstract class PowerUpData : ScriptableObject
	{
	    public string Name = "Power Up";
	    public float PowerUpVisualMultiplier = 10f;
	    public float BaseAdditiveHealth = 5f;


	    public void Execute(GameObject obj, float power, MessageManager mm)
	    {
            if (power < 0.1f)
            {
                power = .1f;
            }

            if (mm != null)
	        {
	            mm.SetMessage(Name + " (Lv." + (power*PowerUpVisualMultiplier) + " Power Up)", 2f);
	        }

            obj.SendMessage("UpgradeHealth", BaseAdditiveHealth);
            ApplyPowerUp(obj, power);
	    }

        

	    protected abstract void ApplyPowerUp(GameObject obj, float power);
	}
}

