﻿using System;
using UnityEngine;

namespace BossMan
{
    /// <summary>
    /// Health data
    /// </summary>
    [CreateAssetMenu(fileName = "DamageData", menuName = "PowerUp/Damage")]
    public class PuDamageData : PowerUpData
    {
        public float AdditiveDamage = 20f;

        protected override void ApplyPowerUp(GameObject obj, float power)
        {

            obj.SendMessage("UpgradeDamage", AdditiveDamage * power);

        }
    }
}

