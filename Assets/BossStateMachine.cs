﻿using UnityEngine;
using System.Collections.Generic;

namespace BossMan
{
	/// <summary>
	/// 
	/// </summary>
	public class BossStateMachine : MonoBehaviour
    {
        public enum BossStates
        {
            Idle,
            Jump,
            Attack,
        }

        private readonly Dictionary<BossStates, string> AnimationStateMapper = new Dictionary<BossStates, string>()
        {
            {
                BossStates.Idle, "Idle"
            },
            {
                BossStates.Jump, "Jump"
            },
            {
                BossStates.Attack, "Attack"
            },
        };

        public BossStates State = BossStates.Idle;

        public bool CanInteract
        {
            get { return State == BossStates.Idle; }
        }

        [SerializeField]
        private Animator _anim;

        // Use this for initialization
        void Start()
        {
            SwitchState(BossStates.Idle);

        }

        // Update is called once per frame
        void FixedUpdate()
        {

        }

        public bool SwitchState(BossStates state)
        {
            if (_anim == null)
                return false;

            if (State != state)
            {
                _anim.SetBool(AnimationStateMapper[State], false);
                _anim.SetBool(AnimationStateMapper[state], true);
                State = state;

                return true;
            }

            return false;
        }
    }
}

