﻿using System;
using UnityEngine;

namespace BossMan
{
    /// <summary>
    /// Health data
    /// </summary>
    [CreateAssetMenu(fileName = "FireRateData", menuName = "PowerUp/FireRate")]
    public class PuFireRateData : PowerUpData
    {
        public float AdditiveFireRate = 1f;

        protected override void ApplyPowerUp(GameObject obj, float power)
        {
            obj.SendMessage("UpgradeFireRate", AdditiveFireRate * power);
        }
    }
}

