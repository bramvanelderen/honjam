﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace BossMan
{
    /// <summary>
    /// Base powerup class
    /// </summary>
    public class PowerUp : MonoBehaviour
    {
        public PowerUpData data;
        public float Power = 1f;
        public float Lifetime = 30f;
        public bool IsUsed = false;
        public float UsedCount = 0;
        public MessageManager Mm;
        public CustomClip PickUpClip;
        public CustomClip DestroyClip;

        private GameObject _player = null;

        void Update()
        {
            if (_player != null)
            {
                if (Input.GetButtonDown("DestroyBody"))
                {
                    var obj = new GameObject();
                    DestroyClip.Play(obj.AddComponent<AudioSource>());
                    Destroy(gameObject);
                }
            }
        }


        public void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.tag != "Player")
                return;

            _player = collision.gameObject;

            if (IsUsed)
                return;

            
            if (data != null)
            {
                var obj = new GameObject();
                PickUpClip.Play(obj.AddComponent<AudioSource>());
                Destroy(obj, 2f);
                data.Execute(collision.transform.gameObject, Power, Mm);
            }

            IsUsed = true;
            UsedCount++;
        }

        public void OnTriggerExit2D(Collider2D collision)
        {
            if (_player != null && _player == collision.gameObject)
            {
                _player = null;
            }
        }
    }
}

